<?php

use Silex\Provider\MonologServiceProvider;

// Enable the debug mode.
$app['debug'] = true;

// RabbitMQ server configuration.
$app['queue_host'] = 'localhost';
$app['queue_port'] = 5672; // Default 5672.
$app['queue_user'] = 'guest';
$app['queue_pass'] = 'guest';
$app['queue_name'] = 'queue_name';

$app->register(new MonologServiceProvider(), array(
    'monolog.logfile' => __DIR__.'/../var/logs/silex_dev.log',
));
